#model.py
```
class OrgMember(models.Model):
    org_poc = models.CharField(max_length=100, blank=False, verbose_name="Organization POC")
    org_name = models.CharField(max_length=100, blank=False, verbose_name="Organisation Name")
    phone = models.CharField(max_length=10, blank=False, verbose_name="Phone Number")
    email = models.EmailField(blank=False, unique=True, verbose_name="Email ID")

    def __str__(self):
        return self.email
```

#forms.py
```
class OrgMembersForm(forms.ModelForm):
    class Meta:
        model = OrgMember
        fields = ['org_poc', 'org_name', 'phone', 'email']
```

#views.py
```
def org_member_view(request):
    if request.method == "POST":
        form = OrgMembersForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Member Added Successfully.")
            return redirect('users-members')
        else:
            messages.error(request, "Please Try Again.")
            print(form.is_valid())
            print(form.errors)
    else:
        form = OrgMembersForm()
    members = OrgMember.objects.all()
    context = {'form': form, 'members': members}
    return render(request, 'users/members.html', context)

def uploadcsv(request):
    data = {}
    if "GET" == request.method:
        return render(request, "users/members.html", data)
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_files"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request,'File is not CSV type')
            return HttpResponseRedirect(reverse("users-members"))
        #if file is too large, return
        if csv_file.multiple_chunks():
            messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return HttpResponseRedirect(reverse("users-members"))

        file_data = csv_file.read().decode("utf-8")

        lines = file_data.split("\n")
        #loop over the lines and save them in db. If error , store as string and then display
        for line in lines:
            fields = line.split(",")
            data_dict = {}
            data_dict["org_poc"] = fields[0]
            data_dict["org_name"] = fields[1]
            data_dict["phone"] = fields[2]
            data_dict["email"] = fields[3]
            try:
                form = OrgMembersForm(data_dict)
                if form.is_valid():
                    form.save()
                else:
                    logging.getLogger("error_logger").error(form.errors.as_json())
            except Exception as e:
                logging.getLogger("error_logger").error(form.errors.as_json())
                pass

    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. "+repr(e))
        messages.error(request,"Unable to upload file. "+repr(e))

    return HttpResponseRedirect(reverse("users-members"))
```

#template.html
```
{% extends "base.html" %}
{% load widget_tweaks %}

{% block content %}
<div class="container-fluid">
  <div class="">
    <section class="section pb-5">
      <button class="btn purple-gradient btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#fluidModalRightSuccessDemo">Add a member</button>
      <button class="btn purple-gradient btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#fluidModalRightSuccessDemo1">Bulk Add</button>
      <br>
      <!--Section heading -->
      <!--<h2 class="text-center my-5 h1 pt-4">Book for Visitor</h2>-->
    </section>
    <section class="section">
      <div class="card">
        <div class="card-body">
          <!-- Table -->
          <table class="table table-responsive">
            <thead>
              <tr>
                <th>#</th>
                <th class="th-lg">Organization POC</th>
                <th class="th-lg">Organization Name</th>
                <th class="th-lg">Phone</th>
                <th class="th-lg">Email</th>
              </tr>
            </thead>
            <tbody>
              {% for member in members %}
              <tr>

                <th scope="row">{{ forloop.counter }}</th>
                <td>{{ member.org_poc }}</td>
                <td>{{ member.org_name }}</td>
                <!--              <td>Table cell</td>-->
                <td>{{ member.phone }}</td>
                <td>{{ member.email }}</td>

              </tr>
              {% endfor %}
            </tbody>
          </table>
          <!-- Table -->
        </div>
      </div>
    </section>
  </div>
</div>
{% endblock content %}
<!-- Main layout -->
{% block modal %}
<!-- Full Height Modal Right Success Demo -->
<div class="modal fade right" id="fluidModalRightSuccessDemo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true" data-backdrop="false">
<div class="modal-dialog modal-full-height modal-right modal-notify modal-success" role="document">
  <!-- Content -->
  <div class="modal-content">
    <!-- Header -->
    <div class="modal-header">
      <p class="heading lead">Add Member</p>

      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="white-text">&times;</span>
      </button>
    </div>

    <!-- Body -->
    <!--Form with header -->
    <form method="POST" action="" enctype="multipart/form-data">
      {% csrf_token %}

      {% for hidden in form.hidden_fields %} {{ hidden }} {% endfor %}
      {% if form.non_field_errors %}
      <div class="alert alert-danger" role="alert">
        {% for error in form.non_field_errors %} {{ error }} {% endfor %}
      </div>
      {% endif %}

      <div class="card">

        <div class="card-body">
          <!--Header -->
          <!--Body -->
          <div class="md-form">
            <i class="fas fa-user prefix grey-text"></i>
            <!-- <input type="text" id="form-name" class="form-control"> -->
            {% render_field form.org_poc class="form-control" id="form-org_poc" type="text"%}
              {% for error in field.errors %}
                <span class="help-block">{{ error }}</span>
              {% endfor %}
            <label for="form-name">Organization POC</label>
          </div>
          <div class="md-form">
            <i class="fas fa-user prefix grey-text"></i>
            <!-- <input type="text" id="form-name" class="form-control"> -->
            {% render_field form.org_name class="form-control" id="form-org_name" type="text"%}
            {% for error in field.errors %}
              <span class="help-block">{{ error }}</span>
            {% endfor %}
            <label for="form-name">Organization Name </label>
          </div>
          <div class="md-form">
            <i class="fas fa-user prefix grey-text"></i>
            <!-- <input type="text" id="form-name" class="form-control"> -->
            {% render_field form.phone class="form-control" id="form-phone" type="text"%}
            {% for error in field.errors %}
              <span class="help-block">{{ error }}</span>
            {% endfor %}
            <label for="form-name">Phone</label>
          </div>

          <div class="md-form">
            <i class="fas fa-envelope prefix grey-text"></i>
            <!-- <input type="text" id="form-email" class="form-control"> -->
            {% render_field form.email class="form-control" id="form-email" type="text"%}
            {% for error in field.errors %}
              <span class="help-block">{{ error }}</span>
            {% endfor %}
            <label for="form-email">Email </label>
          </div>
        </div>
      </div>
      <!--Form with header -->

      <!-- Footer -->
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-success">Submit <i class="far fa-gem ml-1"></i></button>
        <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Cancel</a>
      </div>
    </form>
  </div>
  <!-- Content -->
</div>
</div>
<!-- Full Height Modal Right Success Demo -->



<!-- Full Height Modal Right Success Demo -->
<div class="modal fade right" id="fluidModalRightSuccessDemo1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
aria-hidden="true" data-backdrop="false">
<div class="modal-dialog modal-full-height modal-right modal-notify modal-success" role="document">
  <!-- Content -->
  <div class="modal-content">
    <!-- Header -->
    <div class="modal-header">
      <p class="heading lead">Org Members</p>

      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="white-text">&times;</span>
      </button>
    </div>
    <form action="{% url 'users-members' %}" method="POST" enctype="multipart/form-data" class="form-horizontal">
    {% csrf_token %}
    <div class="form-group">
        <label for="name" class="col-md-3 col-sm-3 col-xs-12 control-label">File: </label>
        <div class="col-md-8">
            <input type="file" name="csv_file" id="csv_file" required="True" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3" style="margin-bottom:10px;">
             <button class="btn btn-primary" name="btnform2"> <span class="glyphicon glyphicon-upload" style="margin-right:5px;"></span>Upload </button>
        </div>
    </div>
    </form>
  </div>
  <!-- Content -->
</div>
</div>
<!-- Full Height Modal Right Success Demo -->
{% endblock modal %}
```

